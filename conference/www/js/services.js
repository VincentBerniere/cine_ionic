angular.module('starter.services', ['ngResource'])
  .factory('serviceMovie', function ($http) {
    return{
      search: function(query, page){
        return $http.get("http://localhost:3002/search?q=" + query + "&page=" + page);
      },
      info: function(id){
        return $http.get("http://localhost:3002/info/" + id);
      },
      popular: function(page){
        return $http.get("http://localhost:3002/popular?page=" + page);
      },
      similar: function (id, page) {
        return $http.get("http://localhost:3002/similar/" + id + "?page=" + page);
      }

    }
  });
