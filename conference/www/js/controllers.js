angular.module('starter.controllers', ['starter.services'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PopularCtrl', function ($scope, serviceMovie) {
  $scope.currentPage = 1;
  $scope.totalPages = 0;
  $scope.loading = true;
  $scope.orderByPredicate = "title";
  $scope.orderByReverse = false;

  $scope.loadMovies = function(){
    $scope.loading = true;
    serviceMovie.popular($scope.currentPage).success(function(data){
      $scope.loading = false;
      $scope.movies = data.results;
      $scope.totalPages = data.total_pages;
    });
  };

  $scope.pageChanged = function(){
    $scope.loadMovies();
  };

  $scope.clickPredicateName = function(){
    $scope.orderByReverse = !$scope.orderByReverse;
    $scope.orderByPredicate = 'title';
  }

  $scope.clickPredicateRate = function(){
    $scope.orderByReverse = !$scope.orderByReverse;
    $scope.orderByPredicate = 'vote_average';
  }

  $scope.loadMovies();
});
